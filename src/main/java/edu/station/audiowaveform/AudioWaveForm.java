package edu.station.audiowaveform;

import java.io.*;
import java.util.ArrayList;
import java.util.UUID;

public class AudioWaveForm {

    private String binPath;
    private String tmpDir;

    public AudioWaveForm() throws IOException {
        InputStream is = getClass().getClassLoader().getResourceAsStream("audiowaveform");
        File file = File.createTempFile("audiowaveform", "");
        file.setExecutable(true);
        file.setReadable(true);
        OutputStream outputStream = new FileOutputStream(file);

        byte[] b = new byte[2048];
        int length;

        while ((length = is.read(b)) != -1) {
            outputStream.write(b, 0, length);
        }

        is.close();
        outputStream.close();
        binPath = file.getPath();
        Runtime.getRuntime().exec("chmod +x "+binPath);

        tmpDir=System.getProperty("java.io.tmpdir");
    }

    public AudioWaveForm(String path) throws IOException {
        this();
        binPath = path;
    }

    public String genData(String path) throws IOException, InterruptedException, AudioWaveFormException {
        ArrayList<String> cmd = new ArrayList<>();
        String pathToDat = tmpDir+ File.separator + UUID.randomUUID().toString().replaceAll("-", "")+".dat";
        cmd.add(binPath);
        cmd.add("-i"+path);
        cmd.add("-o"+pathToDat);
        cmd.add("-z256");
        cmd.add("-b8");

        ProcessBuilder processBuilder = new ProcessBuilder(cmd);
        Process p = processBuilder.start();

        p.waitFor();
        if(p.exitValue()==0) return pathToDat;

        BufferedReader reader =  new BufferedReader(new InputStreamReader(p.getErrorStream()));
        String line = "";
        StringBuilder sb= new StringBuilder();
            while ((line = reader.readLine())!= null) {
                sb.append(line + "\n");
        }

        throw new AudioWaveFormException(sb.toString());

    }

    public String genChart(String path, int secDuration) throws IOException, InterruptedException, AudioWaveFormException {
        ArrayList<String> cmd = new ArrayList<>();
        String pathToDat = tmpDir+ File.separator + UUID.randomUUID().toString().replaceAll("-", "")+".png";
        cmd.add(binPath);
        cmd.add("-i"+path);
        cmd.add("-o"+pathToDat);
        cmd.add("-e"+secDuration);
        cmd.add("-w1000");
        cmd.add("-h200");
        ProcessBuilder processBuilder = new ProcessBuilder(cmd);
        Process p = processBuilder.start();

        p.waitFor();
        if(p.exitValue()==0) return pathToDat;

        BufferedReader reader =  new BufferedReader(new InputStreamReader(p.getErrorStream()));
        String line = "";
        StringBuilder sb= new StringBuilder();
        while ((line = reader.readLine())!= null) {
            sb.append(line + "\n");
        }

        throw new AudioWaveFormException(sb.toString());
    }
}

package edu.station.audiowaveform;

public class AudioWaveFormException extends Exception {
    public AudioWaveFormException() { super(); }
    public AudioWaveFormException(String message) { super(message); }
    public AudioWaveFormException(String message, Throwable cause) { super(message, cause); }
    public AudioWaveFormException(Throwable cause) { super(cause); }
}

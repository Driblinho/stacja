package edu.station.controller.home;

import edu.station.audiowaveform.AudioWaveForm;
import edu.station.audiowaveform.AudioWaveFormException;
import edu.station.player.PlayerStatus;
import edu.station.player.UIEqualizerBand;
import javafx.beans.value.ChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.media.AudioEqualizer;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.FileChooser;
import javafx.util.Duration;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class Home implements Initializable {

    private int status;

    @FXML
    private Button load,play,stop;
    private FileChooser fileChooser;
    private File file;
    private List<Media> mediaList;
    private List<MediaPlayer> playerList;
    @FXML
    private ProgressBar progress;
    @FXML
    private VBox trackList;

    @FXML
    private Slider eq1sc,eq2sc,eq3sc,eq4sc,eq5sc,eq6sc,eq7sc,eq8sc,eq9sc,eq10sc;
    @FXML
    private Label eq1txt,eq2txt,eq3txt,eq4txt,eq5txt,eq6txt,eq7txt,eq8txt,eq9txt,eq10txt;
    @FXML
    private TextField eq1input,eq2input,eq3input,eq4input,eq5input,eq6input,eq7input,eq8input,eq9input,eq10input;

    private List<UIEqualizerBand> bandList;

    private AudioEqualizer audioEqualizer;


    private MediaPlayer mediaPlayer = null;

    private ChangeListener<Duration> progressListner;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        fileChooser = new FileChooser();
        mediaList = new ArrayList<>();
        playerList = new ArrayList<>();
        status = PlayerStatus.STOP;


        bandList = new ArrayList<>();
        bandList.add(new UIEqualizerBand(eq1sc,eq1txt,eq1input));
        bandList.add(new UIEqualizerBand(eq2sc,eq2txt,eq2input));
        bandList.add(new UIEqualizerBand(eq3sc,eq3txt,eq3input));
        bandList.add(new UIEqualizerBand(eq4sc,eq4txt,eq4input));
        bandList.add(new UIEqualizerBand(eq5sc,eq5txt,eq5input));
        bandList.add(new UIEqualizerBand(eq6sc,eq6txt,eq6input));
        bandList.add(new UIEqualizerBand(eq7sc,eq7txt,eq7input));
        bandList.add(new UIEqualizerBand(eq8sc,eq8txt,eq8input));
        bandList.add(new UIEqualizerBand(eq9sc,eq9txt,eq9input));
        bandList.add(new UIEqualizerBand(eq10sc,eq10txt,eq10input));



        load.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                file = fileChooser.showOpenDialog(null);
                if(file==null) return;
                Media media = new Media(file.toURI().toString());
                MediaPlayer loc = new MediaPlayer(media);

                loc.totalDurationProperty().addListener((observable, oldValue, newValue) -> {

                    String imgP="";
                    try {
                        AudioWaveForm aw = new AudioWaveForm();

                        imgP = aw.genChart(aw.genData(file.getPath()),(int) newValue.toSeconds());
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (AudioWaveFormException e) {
                        e.printStackTrace();
                    }

                    File f = new File(imgP);
                    Image img = new Image(f.toURI().toString());

                    trackList.getChildren().add(new ImageView(img));
                    //Usuń listner dla progressbar
                    if(progressListner!=null)
                        mediaPlayer.currentTimeProperty().removeListener(progressListner);

                    if(mediaPlayer==null || newValue.greaterThan(mediaPlayer.getTotalDuration())) {
                        mediaPlayer = loc;
                        //Nowy litner dla progres bar
                        progressListner = progress(newValue.toMillis());
                        mediaPlayer.currentTimeProperty().addListener(progressListner);

                        audioEqualizer = mediaPlayer.getAudioEqualizer();
                        audioEqualizer.setEnabled(true);
                        final int[] i = {0};
                        audioEqualizer.getBands().forEach(equalizerBand -> {
                            equalizerBand.setGain(bandList.get(i[0]).getGian());
                            bandList.get(i[0]).setEqualizerBand(equalizerBand);
                            i[0]++;
                        });

                    }
                });
                playerList.add(loc);
            }
        });



        play.setOnAction(event -> {
            if(status==PlayerStatus.STOP || status==PlayerStatus.PAUSE) {
                playerList.forEach(MediaPlayer::play);
                status = PlayerStatus.PLAY;
            } else if(status==PlayerStatus.PLAY) {
                playerList.forEach(MediaPlayer::pause);
                status = PlayerStatus.PAUSE;
            }
        });



        stop.setOnAction(event -> {
            if(status!=PlayerStatus.STOP) {
                playerList.forEach(MediaPlayer::stop);
                progress.setProgress(0d);
                status = PlayerStatus.STOP;
            }
        });

    }

    /**
     * Update progressBar
     * @param total
     * @return
     */
    private ChangeListener<Duration> progress(double total) {
        return (observable, oldValue, newValue) -> progress.setProgress(newValue.toMillis()/total);
    }

}

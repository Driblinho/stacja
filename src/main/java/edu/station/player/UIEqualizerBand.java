package edu.station.player;

import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.media.EqualizerBand;

public class UIEqualizerBand {

    private Slider slider;
    private Label label;
    private TextField text;
    private EqualizerBand equalizerBand;

    public UIEqualizerBand(Slider slider, Label label, TextField text) {
        equalizerBand = null;
        this.slider = slider;
        slider.setValue(0d);
        this.label = label;
        this.text = text;
        text.setText("0");
        slider.valueProperty().addListener((observable, oldValue, newValue) -> {
            text.setText(newValue.toString());
            if(equalizerBand!=null) equalizerBand.setGain(newValue.doubleValue());
        });
        text.setDisable(true);
        slider.setMin(EqualizerBand.MIN_GAIN);
        slider.setMax(EqualizerBand.MAX_GAIN);
    }

    public void setEqualizerBand(EqualizerBand equalizerBand) {
        this.equalizerBand = equalizerBand;
        double frq = equalizerBand.getCenterFrequency();
        String str = "";
        if(frq>=1000) {
            str+=(int)(frq/1000)+" kHz";
        } else str+=(int)frq+" Hz";
        label.setText(str);

    }

    public double getGian() {
        return slider.getValue();
    }

    public Label getLabel() {
        return label;
    }

    public TextField getText() {
        return text;
    }

    public EqualizerBand getEqualizerBand() {
        return equalizerBand;
    }
}
